This website is not AODO compliant. 
The reasons for this are:
1. Background color makes text hard to read
2. Link has font that is too small to reasonably read
3. Picture is too small to reasonably see and no alt text