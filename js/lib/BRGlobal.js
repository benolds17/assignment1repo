//BRGlobal.js
//Benjamin Reynolds
//Global functions

//hide add fields on add page
$(function addHideFields() {

    $("#brHiddenFields").hide();

    $("#chkRatingVisible").click(function () {
        if ($(this).is(":checked")) {
            $("#brHiddenFields").show();
        } else {
            $("#brHiddenFields").hide();
        }
    });
});

//Add overall rating and get the average between values
function addOverallRating(){

    //get input from text boxes
    var foodRating = 0;
    var serviceRating = 0;
    var valueRating = 0;

    foodRating = document.getElementById("txtFoodRating").value;
    serviceRating = document.getElementById("txtServiceRating").value;
    valueRating = document.getElementById("txtValueRating").value;

    //calculate overall rating
    var total = 0;
    total = (+foodRating + +serviceRating + +valueRating)*100/15;

    //round total and parse to string
    var stringTotal = String(Math.round(total));

    //set value of total to the textbox of overall rating
    document.getElementById('txtOverallRating').value = stringTotal;
}

//Hide the edit value fields
function editHideFields() {


    console.info("editHideFields();")
    $("#chkEditRatingVisible").click(function () {
        if ($(this).is(":checked")) {
            $("#brEditHiddenFields").show();
        } else {
            $("#brEditHiddenFields").hide();
        }
    });

}

//Hide the edit value fields
function editHideFields_onload() {


    console.info("editHideFields();")
    $("#chkEditRatingVisible").load(function () {
        if ($(this).is(":checked")) {
            $("#brEditHiddenFields").show();
        } else {
            $("#brEditHiddenFields").hide();
        }
    });

}

//Calculate overallRating in edit page
function editOverallRating(){

    //get input from text boxes
    var foodRating = 0;
    var serviceRating = 0;
    var valueRating = 0;

    foodRating = document.getElementById("txtEditFoodRating").value;
    serviceRating = document.getElementById("txtEditServiceRating").value;
    valueRating = document.getElementById("txtEditValueRating").value;

    //calculate overall rating
    var total = 0;
    total = (+foodRating + +serviceRating + +valueRating)*100/15;

    //round total and parse to string
    var stringTotal = String(Math.round(total));

    //set value of total to the textbox of overall rating
    document.getElementById('txtEditOverallRating').value = stringTotal;
}

//view feedback page load
function brViewFeedbackPage_show() {
    brGetReviews();
}

//edit feedback page load
function brEditFeedbackPage_show() {
    editHideFields_onload();
    editOverallRating();
    brShowCurrentReview();
}

function brAddFeedbackPage_show(){
    clearAddForm();
}

//init function for pageshow
function init() {
    console.info("DOM is ready");


    $("#BRViewFeedbackPage").on("pageshow", brViewFeedbackPage_show);
    $("#BREditFeedbackPage").on("pageshow", brEditFeedbackPage_show);
    $("#BRAddFeedbackPage").on("pageshow", brAddFeedbackPage_show);


}

//Database code
function initDB() {

    try {
        DB.brCreateDatabase();
        if (db) {


            console.info("Creating Tables....");
            DB.brCreateTables();
        }
        else {
            console.error("Error: cannot create Db. can not proceed.");
        }

    } catch (e) {
        console.error("Error: (Fatal) Error in initDB(). can not proceed.");
    }
}

//Run when program starts
$(document).ready(function () {
    initDB();
    init();
});
